<?php

namespace RestNotifications\Channels;


use RestNotifications\Notification;
use GuzzleHttp\Client;

class RestBaseChannel
{
    /**
     * @param $notifiable
     * @param Notification $notification
     */
    public function send($notifiable, Notification $notification)
    {

        $params = [
            'form_params' => $notification->toRestService()
        ];

        if($notification->isSecured()){
            $params['headers'] = [
                'Authorization' => 'Bearer '.$notification->getJWTToken()
            ];
        }

        (new Client())->post($notification->getServiceUrl(), $params);
    }
}