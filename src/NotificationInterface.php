<?php

namespace RestNotifications;


interface NotificationInterface
{


    /**
     * Get the array representation of the notification.
     *
     * @return array
     */
    public function toRestService();

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function getServiceUrl();
}
