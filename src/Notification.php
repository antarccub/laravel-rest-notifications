<?php

namespace RestNotifications;


use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification as LaravelNotification;
use RestNotifications\Channels\RestBaseChannel;

class Notification extends LaravelNotification implements NotificationInterface
{

    use Queueable;

    private $token;

    private $service_url;

    /**
     * Create a new notification instance.
     *
     * @param $token
     * @param $jwt_token
     */
    public function __construct($token, $jwt_token = null)
    {
        $this->token = $jwt_token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [RestBaseChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param $notifiable
     * @return array
     */
    public function toRestService()
    {

        return [
            'type' => 'sample-notification-type',
            'mail' => [
                'header' => 'Example Header',
                'first-line' => 'First line of email message',
                'action' => [
                    'text' => 'This is an action button text',
                    'url' => 'http://iam.simolab.com'
                ],
            ],
            ''
        ];
    }

    /**
     * @return string
     */
    public function getServiceUrl()
    {
       return '';
    }

    public function getJWTToken(){
        return $this->token;
    }

    public function isSecured(){
        return $this->getJWTToken() == null;
    }
}